import argparse

PYGANOS = """
  /$$$$$$  /$$   /$$  /$$$$$$   /$$$$$$  /$$$$$$$   /$$$$$$   /$$$$$$$
 /$$__  $$| $$  | $$ /$$__  $$ |____  $$| $$__  $$ /$$__  $$ /$$_____/
| $$  \ $$| $$  | $$| $$  \ $$  /$$$$$$$| $$  \ $$| $$  \ $$|  $$$$$$
| $$  | $$| $$  | $$| $$  | $$ /$$__  $$| $$  | $$| $$  | $$ \____  $$
| $$$$$$$/|  $$$$$$$|  $$$$$$$|  $$$$$$$| $$  | $$|  $$$$$$/ /$$$$$$$/
| $$____/  \____  $$ \____  $$ \_______/|__/  |__/ \______/ |_______/
| $$       /$$  | $$ /$$  \ $$
| $$      |  $$$$$$/|  $$$$$$/  A simple hiding program.
|__/       \______/  \______/

"""

def pack(file1, file2):
    print(f"\n\nHiding {file2} in {file1}.")
    output = file1
    hide_name = file2
    file1 = open(file1, 'rb').read()
    file2 = open(file2, 'rb').read()
    with open(f'output/{output}', 'wb') as data:
        data.write(file1)
        data.write(f'\npyganos-start\n{hide_name}\n'.encode())
        data.write(file2)
    print("\nDone!")

def unpack(output):
    is_pyganos = False
    with open(output, 'rb') as file:
        searchlines = file.readlines()
        for i, line in enumerate(searchlines):
            if 'pyganos-start'.encode() in line:
                filename = searchlines[i+1].decode().strip()
                unpacked = searchlines[i+2:]
                is_pyganos = True
                print(f"\n\nSuccesfully unhide {filename} - Look into the output folder, there will be {filename}!")
    if is_pyganos == True:
        with open(f'output/{filename}', 'wb') as output:
            for line in unpacked:
                output.write(line)
    else:
        print('\n\nIt isn\'t a pyganos File! Please try an other one.')


cmdinput = argparse.ArgumentParser(description='A program which can hide one file in an other.')
cmdinput.add_argument('--h', '--hide', nargs = 2, help='This command is for packing /hiding the data. You have to insert two files. Eg. python pyganos.py -h file.png file2.txt')
cmdinput.add_argument('--u', '--unhide', nargs = 1, help='This command is for unpacking you hiding data! Usage python pyganos -u file1.png - The output will be saved in the folder output.')
args = vars(cmdinput.parse_args())

if args['h']:
    print(PYGANOS)
    filenames = args['h']
    file1 = filenames[0]
    file2 = filenames[1]
    pack(file1, file2)

elif args['u']:
    print(PYGANOS)
    filename = args['u']
    unpack(filename[0])

else:
    print(PYGANOS)
    while True:
        user = input('Do you like to hide or unhide a file? [H|U]> ')
        if user.upper() == 'H':
            file1 = input('Your first file in which the second will be hided (If not in same dir an pyganos, please give in also the dir!)> ')
            file2 = input('You file which you would like to hide (If not in same dir an pyganos, please give in also the dir!)> ')
            pack(file1, file2)
            break
        elif user.upper() == 'U':
            file = input('Which file you\'d like to unhide?> ')
            unpack(file)
            break
        else:
            print('Not possible!')
